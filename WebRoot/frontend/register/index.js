/**
 * 
 */
var v_registerFrom = {
    data() {
      return {
        active: 0
      };
    },

    methods: {
      next() {
        if (this.active++ > 2) this.active = 0;
      }
    }
  }
var Ctor = Vue.extend(v_registerFrom);
new Ctor().$mount('#registerForm');