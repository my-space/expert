/**
 * 
 */
var v_user = {
    methods: {
    	viewInfo(index, row) {
    		console.table(row.date);
      },
      editInfo(index,row) {
    	  	console.table(row);
        },
      deleteRow(index,row) {
        	row.splice(index, 1);
      },
      findAll(currentPage){
    	  this.listLoading = true;
    	  if (!isNaN(currentPage)) {
    	   this.currentPage = currentPage;
    	  }
    	  var params_ = {
    	   pageNum: this.currentPage,
    	   pageSize: 10
    	  };
    	  if (this.username && this.username.trim() != "") {
    	   params_['CN'] = this.username;
    	  }
//    	  this.$http.get("${basePath}/rdms/searchUser", {
//    	   params: params_
//    	  }).then(function (response) {
//    	   console.log(response.data);
//    	   this.total = response.data.count;
//    	   this.tableData = [];
//    	   for (var key in response.data.list) {
//    	   this.$set(this.tableData, key, response.data.list[key]);
//    	   }
//    	  }).catch(function (response) {
//    	   console.error(response);
//    	  });
    	  $.post("/rdms/searchUser", {params: params_}, function(data) {
				if(data){					  
			    	   this.total = data.count;			    	   
			    	   this.tableData = [];
			    	   console.table(this.tableData);
			    	  for (var key in data.list) 
			    	   { 
			    		   this.$set(this.tableData, key, data.list[key]); 
			    	   }
			    	   
				}else{
					this.$toast('查询错误!')
				}
			});
    	  
    	  this.listLoading = false;
      },
      changeUsername(){
    	  this.findAll(1);
      }
      
    },
    data() {
      return {
        tableData: [],
        currentPage: 1,
        total: 10,
        listLoading: false,
        username:null
      }
    }
  }
var Ctor = Vue.extend(v_user);
new Ctor().$mount('#userlist');
