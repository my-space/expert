package com.rdms.cms.config;

import com.jfinal.config.Routes;
import com.rdms.cms.mvc.controller.ExpertController;
import com.rdms.cms.mvc.controller.FHomeController;
import com.rdms.cms.mvc.controller.RequirmentController;


public class CmsRoutes extends Routes {

	@Override
	public void config() {
		
		add("/",FHomeController.class,"/cms/front");
		add("/exp",ExpertController.class,"/cms/front/expertin");
		add("/req",RequirmentController.class,"/cms/front/require");
	}

}
