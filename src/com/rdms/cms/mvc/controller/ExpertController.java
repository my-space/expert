package com.rdms.cms.mvc.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rdms.cms.mvc.model.ExpertAwards;
import com.rdms.cms.mvc.model.ExpertEdu;
import com.rdms.cms.mvc.model.ExpertInfo;
import com.rdms.cms.mvc.model.ExpertTechAchive;
import com.rdms.cms.mvc.model.PlateDict;
import com.sys.config.BaseController;
import com.sys.tool.StringUtil;
import com.sys.tool.UuidUtil;

public class ExpertController extends BaseController {

	/**
	 * Expert专家管理后台
	 */
	public void expertIndex() {
		if (getSessionUser() != null) {
			setAttr("username", getSessionUser().get("cn"));
			setAttr("sysuser", getSessionUser());
		}
		setAttr("title", "专家系统-专家信息管理");
		render("expertIndex.html");
	}

	/**
	 * 注册信息
	 */
	public void index() {
		if (getSessionUser() != null) {
			setAttr("username", getSessionUser().get("cn"));
			setAttr("sysuser", getSessionUser());
		}
		setAttr("title", "专家系统-专家注册");
		render("expert_register.html");
	}

	/**
	 * 基本信息
	 */
	public void expert_baseinfo() {
		if (getSessionUser() != null) {
			setAttr("username", getSessionUser().get("cn"));
			setAttr("model", ExpertInfo.dao.getBaseInfo(getSessionUser().get("ldap_id")));
		}
		setAttr("fileds", PlateDict.dao.getHanyeDictAll("01"));
		setAttr("domins", PlateDict.dao.getHanyeDictAll("02"));		
		setAttr("title", "专家系统-专家基本信息");
		render("expert_baseinfo.html");
	}
	/**
	 * 专家保存和更新
	 */
	public void expertSave() {
		if (getSessionUser() == null) {
			renderFailed("请先注册或登录！");		
		}
		else{
			ExpertInfo ef=getModel(ExpertInfo.class, "model");
			if(StringUtil.isNotEmpty(ef.getStr("id"))){
				ef.update();
			}
			else{
				ef.set("id", UuidUtil.getUuid(false));
				ef.set("code", getSessionUser().get("ldap_id"));
				ef.set("status", 2);				
				ef.save();
			}
			renderSuccess("保存成功！");		
		}	
	}

	/**
	 * 教育信息页面
	 */
	public void expert_edu() {
		try {
			setAttr("username", getSessionUser().get("cn"));
			setAttr("sysuser", getSessionUser());

		} catch (Exception e) {
			e.getMessage();
		}
		setAttr("title", "专家系统-专家教育信息录入");
		render("expert_education.html");
	}

	/**
	 * 教育信息列表
	 */
	public void eduList() {
		try {
			List<ExpertEdu> eelist = ExpertEdu.dao.getJobList(getSessionUser().get("ldap_id"));

			Map<String, Object> map = new HashMap<String, Object>();
			map.put("rows", eelist);
			map.put("results", eelist.size());
			map.put("hasError", false);
			renderJson(map);
		} catch (Exception e) {
			System.err.println("获取教育信息失败");
			renderNull();
		}

	}

	public void addedu() {
		try {
			ExpertEdu ee = new ExpertEdu();
			ee.set("id", UuidUtil.getUuid(false));
			ee.set("code", getSessionUser().get("ldap_id"));
			ee.set("begintime", getPara("begintime"));
			ee.set("endtime", getPara("endtime"));
			ee.set("schoolname", getPara("schoolname"));
			ee.set("majorname", getPara("majorname"));
			ee.set("xueli", getPara("xueli"));
			ee.set("degree", getPara("degree"));
			ee.save();
			renderSuccess("保存成功！");
		} catch (Exception e) {
			renderFailed("保存失败！");
		}

	}

	public void deledu() {
		// System.out.println(getPara("ids"));
		String[] idlist = getPara("ids").split(",");
		for (int i = 0; i < idlist.length; i++) {
			ExpertEdu.dao.deleteById(idlist[i]);
		}
		renderSuccess("删除成功！");
	}

	public void updateedu() {
		ExpertEdu ee = ExpertEdu.dao.findById(getPara("id"));
		ee.set("begintime", getPara("begintime"));
		ee.set("endtime", getPara("endtime"));
		ee.set("schoolname", getPara("schoolname"));
		ee.set("majorname", getPara("majorname"));
		ee.set("xueli", getPara("xueli"));
		ee.set("degree", getPara("degree"));
		ee.update();
		renderSuccess("保存成功！");
	}

	
	/**
	 * 奖励录入
	 */
	public void expert_awards() {
		try {
			setAttr("username", getSessionUser().get("cn"));

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		setAttr("title", "专家系统-获奖情况录入");
		render("expert_awards.html");
	}

	/**
	 * 教育信息列表
	 */
	public void awardsList() {
		try {
			List<ExpertAwards> eelist = ExpertAwards.dao.getAwardsList(getSessionUser().get("ldap_id"));
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("rows", eelist);
			map.put("results", eelist.size());
			map.put("hasError", false);
			renderJson(map);
		} catch (Exception e) {
			System.err.println("获取奖励信息失败");
			renderNull();
		}

	}

	public void addaward() {
		try {
			ExpertAwards ea = new ExpertAwards();
			ea.set("id", UuidUtil.getUuid(false));
			ea.set("code", getSessionUser().get("ldap_id"));
			ea.set("aauthor", getPara("aauthor"));
			ea.set("aname", getPara("aname"));
			ea.set("arank", getPara("arank"));
			ea.set("atype", getPara("atype"));
			ea.set("alevel", getPara("alevel"));
			ea.set("atime", getPara("atime"));
			ea.save();
			renderSuccess("保存成功！");
		} catch (Exception e) {
			renderFailed("保存失败!");
		}

	}

	public void delaward() {
		String[] idlist = getPara("ids").split(",");
		for (int i = 0; i < idlist.length; i++) {
			ExpertAwards.dao.deleteById(idlist[i]);
		}
		renderSuccess("删除成功！");
	}

	public void updaward() {
		try {
			System.out.println(getParaValues("id"));
			ExpertAwards ea = ExpertAwards.dao.findById(getPara("id"));
			ea.set("aauthor", getPara("aauthor"));
			ea.set("aname", getPara("aname"));
			ea.set("arank", getPara("arank"));
			ea.set("atype", getPara("atype"));
			ea.set("alevel", getPara("alevel"));
			ea.set("atime", getPara("atime"));
			ea.update();
			renderSuccess("保存成功！");
		} catch (Exception e) {
			renderFailed("保存失败!");
		}

	}

	/**
	 * 技术成果录入页面
	 */
	public void expert_techach() {
		try {
			setAttr("username", getSessionUser().get("cn"));

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		setAttr("title", "专家系统-技术成果录入");
		render("expert_techachieve.html");
	}

	/**
	 * 技术成果信息列表
	 */
	public void tachiveList() {
		try {
			List<ExpertTechAchive> eelist = ExpertTechAchive.dao.getTAchiveList(getSessionUser().get("ldap_id"));
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("rows", eelist);
			map.put("results", eelist.size());
			map.put("hasError", false);
			renderJson(map);
		} catch (Exception e) {
		}

	}

	public void addtachive() {
		ExpertTechAchive eta = new ExpertTechAchive();
		eta.set("id", UuidUtil.getUuid(false));
		eta.set("code", getSessionUser().get("ldap_id"));
		eta.set("taname", getPara("taname"));
		eta.set("tatime", getPara("tatime"));
		eta.set("takeytech", getPara("takeytech"));
		eta.set("tacontribution", getPara("tacontribution"));
		eta.set("tathesis", getPara("tathesis"));
		eta.set("tadescri", getPara("tadescri"));
		eta.save();
		renderSuccess("保存成功！");
	}

	public void deltachive() {

		String[] idlist = getPara("ids").split(",");
		for (int i = 0; i < idlist.length; i++) {
			ExpertTechAchive.dao.deleteById(idlist[i]);
		}
		renderSuccess("删除成功！");

	}

	public void updtachive() {
		try {
			ExpertTechAchive eta = ExpertTechAchive.dao.findById(getPara("id"));
			eta.set("taname", getPara("taname"));
			eta.set("tatime", getPara("tatime"));
			eta.set("takeytech", getPara("takeytech"));
			eta.set("tacontribution", getPara("tacontribution"));
			eta.set("tathesis", getPara("tathesis"));
			eta.set("tadescri", getPara("tadescri"));
			eta.update();
			renderSuccess("保存成功！");
		} catch (Exception e) {

			renderNull();
		}

	}
}
