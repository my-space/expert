package com.rdms.cms.mvc.controller;

import java.util.List;

import com.jfinal.kit.LogKit;
import com.jfinal.plugin.activerecord.Record;
import com.rdms.cms.mvc.model.ExpertFavorite;
import com.rdms.cms.mvc.model.ExpertInfo;
import com.rdms.cms.mvc.model.ExpertJob;
import com.rdms.cms.mvc.model.PlateDict;
import com.rdms.cms.mvc.model.RequireApply;
import com.rdms.cms.mvc.model.Requirement;
import com.sys.config.BaseController;
import com.sys.mvc.user.SysUser;
import com.sys.tool.DBTool;
import com.sys.tool.MD5Util;
import com.sys.tool.Pager;
import com.sys.tool.StringUtil;

public class FHomeController extends BaseController {

	public static String error = "";

	/**
	 * 首页
	 */
	public void index() {
		if (getSessionUser() != null) {
			setAttr("username", getSessionUser().get("cn"));
		}
		setAttr("domins", PlateDict.dao.getHanyeDict("02"));
		setAttr("experts", ExpertInfo.dao.getAllExpertList());
		setAttr("title", "专家系统-首页");
		render("index.html");
	}

	/**
	 * 专家查看搜索
	 */
	public void expert() {
		try {
			setAttr("username", getSessionUser().get("cn"));

		} catch (Exception e) {
			System.err.println("没有登陆！");
		}
		Object[] queryParams = getQueryParams();
		String[] properties = (String[]) queryParams[0];
		String[] symbols = (String[]) queryParams[1];
		Object[] values = (Object[]) queryParams[2];
		Pager page = getPager();
		List<Record> list = DBTool.findByMultProperties(null, "t_expert_info", properties, symbols, values, null, page);
		setAttr("curentPage", page.getPage());
		setAttr("experts", list);
		setAttr("title", "专家系统-专家搜索");
		render("expert.html");
	}

	/**
	 * 专家详细页面
	 */
	public void expdetail() {
		try {
			setAttr("username", getSessionUser().get("cn"));

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		ExpertInfo ei = ExpertInfo.dao.findById(getPara("eid"));
		setAttr("exp", ei);
		setAttr("jobs", ExpertJob.dao.getJobsOfExpert(ei.getStr("code")));
		setAttr("title", ei.getStr("name") + "-" + ei.getStr("field") + "查看");
		render("expert_detail.html");
	}

	/**
	 * 专家收藏
	 */
	public void favorite() {
		if (getSessionUser() == null) {
			renderFailed("报名失败，请确认已登录");
		} else {
			String eid = getPara("eid");
			String username = getSessionUser().get("ldap_id");
			ExpertFavorite eff = ExpertFavorite.dao.ifFavorited(eid, username);
			if (eff == null) {
				ExpertFavorite ef = new ExpertFavorite();
				ef.set("e_id", eid);
				try {
					ef.set("username",username);
					ef.save();
					renderSuccess("收藏成功");
				} catch (Exception e) {
					renderFailed("收藏失败，请确认已登录");
				}
			} else {
				renderFailed("请勿重复收藏!");
			}

		}
	}

	/**
	 * 需求查看搜索页面
	 */
	public void request() {
		try {
			setAttr("username", getSessionUser().get("cn"));

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		Object[] queryParams = getQueryParams();
		String[] properties = (String[]) queryParams[0];
		String[] symbols = (String[]) queryParams[1];
		Object[] values = (Object[]) queryParams[2];
		Pager page = getPager();
		List<Record> list = DBTool.findByMultProperties(null, "t_requirement", properties, symbols, values, null, page);
		setAttr("curentPage", page.getPage());
		setAttr("requirements", list);
		setAttr("title", "专家系统-需求搜索");
		render("request.html");
	}

	/**
	 * 需求详细页面
	 */
	public void reqdetail() {
		try {
			setAttr("username", getSessionUser().get("cn"));

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		setAttr("req", Requirement.dao.findById(getPara("reqid")));
		setAttr("applys", (getPara("reqid")));
		setAttr("title", "专家系统-需求查看");
		render("request_detail.html");
	}

	/**
	 * 需求报名申请
	 */
	public void apply() {
		if (getSessionUser() == null) {
			renderFailed("报名失败，请确认已登录");
		} else {
			String rid = getPara("reqid");
			String username = getSessionUser().get("ldap_id");
			RequireApply ras = RequireApply.dao.ifApplyed(rid, username);
			if (ras == null) {
				RequireApply ra = new RequireApply();
				ra.set("r_id", rid);
				try {
					ra.set("username", username);
					ra.save();
					renderSuccess("报名成功");
				} catch (Exception e) {
					renderFailed("报名失败，请确认已登录");
				}
			} else {
				renderFailed("请勿重复报名!");
			}

		}
	}
	/**
	 * 注册用户
	 */
	public void doregister() {

		boolean ret = false;
		try {
			if (StringUtil.equals(getPara("password"), getPara("repassword")) && StringUtil.isNotEmpty("username")) {

				SysUser su = new SysUser();				
				su.set("PASSWORD", MD5Util.md5Digest(getPara("password")));
				su.set("LDAP_ID", getPara("username"));
				su.set("SN", getPara("username"));
				su.set("CN", getPara("nickname"));
				ret = su.save();
				setSessionAttr("sysUser", su);
			} else if (!StringUtil.equals(getPara("password"), getPara("repassword"))) {
				renderFailed("两次密码不一致！");
				return;
			} else {
				renderFailed("用户名密码不能为空！");
				return;
			}
		} catch (Exception e) {
			System.out.println("注册错误" + e.getMessage());
			renderFailed("注册失败！");
		}
		if (ret) {
			renderSuccess("注册成功 ");

		} else {
			renderFailed("注册失败");
		}
	}

	public void dologin() {

		String username = getPara("username");
		String password = getPara("password");
		// String rememberme = getPara("remember");
		/*
		 * boolean remember =false; if(StringUtil.equals(rememberme,"on")){
		 * remember=true; }
		 */
		List<SysUser> list = null;
		try {
			if (StringUtil.isNotEmpty(username)) {
				list = SysUser.dao.findByMultiProperties(new String[] { "LDAP_ID", "PASSWORD" },
						new Object[] { username, MD5Util.md5Digest(password) });
			} else {
				renderFailed("用户名不能为空");
				return;
			}

		} catch (Exception e) {
			renderFailed("登陆失败");
		}
		if (list.size() > 0) {
			setSessionAttr("sysUser", list.get(0));
			renderSuccess("登陆成功");
		} else {
			renderFailed("用户名或密码错误！");
		}

	}

	/**
	 * 退出系统
	 */
	public void doLogout() {
		try {
			removeSessionAttr("sysUser");
		} catch (Exception e) {
			LogKit.error("未登录，或登出失败");
		}
		redirect("/");
	}

}
