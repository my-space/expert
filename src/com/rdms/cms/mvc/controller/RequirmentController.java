package com.rdms.cms.mvc.controller;

import com.rdms.cms.mvc.model.Requirement;
import com.sys.config.BaseController;

public class RequirmentController extends BaseController {
	
	public void index(){
		
		render("require_info.html");
	}
	public void requireSave(){
		Requirement ee= new Requirement();		
		ee.set("rname", getPara("model.rname"));
		ee.set("raddress", getPara("model.raddress"));
		ee.set("restimate", getPara("model.restimate"));
		ee.set("rduration", getPara("model.rduration"));
		ee.set("rvalidity", getPara("model.rvalidity"));
		ee.set("rcompany", getPara("model.rcompany"));	
		ee.set("rremarks", getPara("model.rremarks"));
		ee.set("rdesc", getPara("model.rdesc"));
		ee.set("rstatus",1);	
		ee.save();
		renderSuccess("操作成功！");
		
	}
}
