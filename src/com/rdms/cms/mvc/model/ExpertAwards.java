package com.rdms.cms.mvc.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

@SuppressWarnings("serial")
public class ExpertAwards extends Model<ExpertAwards> {

	public static final ExpertAwards dao = new ExpertAwards();

	public List<ExpertAwards> getAwardsList(String username) {
		String sql ="select * from t_expert_award  where code=?";
		return find(sql, username);
	}
}
