package com.rdms.cms.mvc.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

public class ExpertEdu extends Model<ExpertEdu> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 930360040091574211L;
	public static final ExpertEdu dao =new ExpertEdu();
	public List<ExpertEdu> getJobList(String username) {
		String sql ="select * from t_expert_edu where code=?";
		return find(sql, username);
	}

}
