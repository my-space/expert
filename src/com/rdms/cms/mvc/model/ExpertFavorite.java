/**  
* @authorxpg
* @date 2017年8月7日-下午9:24:14
* @version V1.0  
*/ 
package com.rdms.cms.mvc.model;

import com.jfinal.plugin.activerecord.Model;

/**
 * @author xpg
 *
 */
@SuppressWarnings("serial")
public class ExpertFavorite extends Model<ExpertFavorite> {

	public static final ExpertFavorite dao =new ExpertFavorite();

	public ExpertFavorite ifFavorited(String eid, String username) {
		String sql ="select * from t_expert_favorite where e_id=? and username=?";
		return ExpertFavorite.dao.findFirst(sql, eid,username);
	}

}
