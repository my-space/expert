package com.rdms.cms.mvc.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;


public class ExpertInfo extends Model<ExpertInfo> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8276886707191044841L;
	public static final ExpertInfo dao =new ExpertInfo();
	public List<ExpertInfo> getAllExpertList(String key ,String fields) {
		
		String sql="select * from t_expert_info where intro like ? or field=?";
		
		return ExpertInfo.dao.find(sql,key,fields);
	}
	public ExpertInfo getBaseInfo(String username) {
		String sql="select * from t_expert_info where code=?";
		return ExpertInfo.dao.findFirst(sql,username);
	}
	public List<ExpertInfo> getAllExpertList(){

		String sql="select top 10 * from t_expert_info";
		
		return ExpertInfo.dao.find(sql);
	}
}
