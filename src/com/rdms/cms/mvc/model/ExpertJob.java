package com.rdms.cms.mvc.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

@SuppressWarnings("serial")
public class ExpertJob extends Model<ExpertJob> {
	public static final ExpertJob dao =new ExpertJob();
	
	
	public List<ExpertJob> getJobsOfExpert(String code){
		String sql="select * from t_expert_job where code=?";	
		return ExpertJob.dao.find(sql,code);
		
	}

}
