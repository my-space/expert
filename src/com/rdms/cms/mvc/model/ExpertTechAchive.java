package com.rdms.cms.mvc.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

@SuppressWarnings("serial")
public class ExpertTechAchive extends Model<ExpertTechAchive> {

	public static final ExpertTechAchive dao = new ExpertTechAchive();

	public List<ExpertTechAchive> getTAchiveList(String username) {
		String sql ="select * from t_expert_techachieve where code=?";
		return find(sql, username);
	}

}
