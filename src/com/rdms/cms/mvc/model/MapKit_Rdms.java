package com.rdms.cms.mvc.model;


import com.jfinal.plugin.activerecord.ActiveRecordPlugin;

public class MapKit_Rdms{
	public static void mapping(ActiveRecordPlugin arp) {		
		arp.addMapping("t_expert_info",ExpertInfo.class);
		arp.addMapping("t_expert_edu",ExpertEdu.class);
		arp.addMapping("t_expert_award",ExpertAwards.class);
		arp.addMapping("t_expert_techachieve",ExpertTechAchive.class);
		arp.addMapping("t_expert_job",ExpertJob.class);
		
		arp.addMapping("t_requirement",Requirement.class);
		arp.addMapping("t_requirement_apply",RequireApply.class);
		
		arp.addMapping("t_expert_favorite",ExpertFavorite.class);
		arp.addMapping("t_plate_dic",PlateDict.class);
		
	}
}
