package com.rdms.cms.mvc.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

@SuppressWarnings("serial")
public class PlateDict extends Model<PlateDict> {
	public static final PlateDict dao =new PlateDict();
	/**
	 * 返回产业和板块信息
	 * @param cat 01产业，02板块
	 * @return
	 */
	public List<PlateDict> getHanyeDict(String cat){
		String sql="select top 9 plate_no,plate_name from t_plate_dic where plate_cat=?";
		return PlateDict.dao.find(sql,cat);
	}
	public List<PlateDict> getHanyeDictAll(String cat) {
		String sql="select plate_no,plate_name from t_plate_dic where plate_cat=?";
		return PlateDict.dao.find(sql,cat);
	}	
}
