package com.rdms.cms.mvc.model;

import com.jfinal.plugin.activerecord.Model;

@SuppressWarnings("serial")
public class RequireApply extends Model<RequireApply> {
	public static final RequireApply dao =new RequireApply();

	public RequireApply ifApplyed(String rid, String username) {
		String sql="select * from t_requirement_apply where r_id=? and username=?";
		return RequireApply.dao.findFirst(sql, rid,username);
	}

}
