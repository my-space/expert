package com.rdms.cms.mvc.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

@SuppressWarnings("serial")
public class Requirement extends Model<Requirement> {
	
	public static final Requirement dao =new Requirement();
	
	public List<Requirement> getAllRequirementList(){
		String sql="select * from t_requirement where rstatus=1";
		return Requirement.dao.find(sql);
	}

}
