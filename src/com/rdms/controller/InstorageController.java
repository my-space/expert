package com.rdms.controller;

import java.util.HashMap;
import java.util.List;

import com.jfinal.core.Controller;
import com.rdms.model.InstorageModel;

public class InstorageController extends Controller {
	
	
	
	
	public void listData(){
		getResponse().addHeader("Access-Control-Allow-Origin", "*");		
		List<InstorageModel> instorage =InstorageModel.dao.find("select * from wms_instorage");
		HashMap<String, Object> hm =new HashMap<String, Object>();
		hm.put("wmsinstorages",instorage);
		hm.put("total",instorage.size());
		renderJson(hm);
	}
    public void add(){
    	getResponse().addHeader("Access-Control-Allow-Origin", "*");    	
    	//System.out.println(getPara("wmsinstorage"));
    	getModel(InstorageModel.class,"instorageModel").remove("id").save();
    	//System.out.println(getPara("id").toString());
    	HashMap<String, Object> hm =new HashMap<String, Object>();
		hm.put("result","success");
		hm.put("msg","成功");
		renderJson(hm);
    }
    public void update(){
    	getResponse().addHeader("Access-Control-Allow-Origin", "*");    	
    	//System.out.println(getPara("wmsinstorage"));
    	getModel(InstorageModel.class,"instorageModel").update();
    	//System.out.println(getPara("id").toString());
    	HashMap<String, Object> hm =new HashMap<String, Object>();
		hm.put("result","success");
		hm.put("msg","成功");
		renderJson(hm);
    }
    public void delete(){
    	getResponse().addHeader("Access-Control-Allow-Origin", "*");  
    	String[] ids = getPara("id").split(",");
		for (String id : ids) {
			new InstorageModel().set("id", id).delete();			
		}
		HashMap<String, Object> hm =new HashMap<String, Object>();
		hm.put("result","success");
		hm.put("msg","成功");
		renderJson(hm);
    }
}
