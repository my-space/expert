package com.rdms.controller;

import java.util.List;

import com.jfinal.plugin.activerecord.Record;
import com.sys.config.BaseController;
import com.sys.tool.DBTool;
import com.sys.tool.StringUtil;


public class RegisterController extends BaseController {
	
	
	public void register(){		
		render("register/index.html");
	}
	public void saveUser(){
		
	}
	public void usercount(){
		render("usercount/index.html");
	}
	public void userList(){
		render("userlist/index.html");
	}
	public void searchUser(){
		Object[] queryParams = getQueryParams();
		String[] properties = (String[]) queryParams[0];
		String[] symbols = (String[]) queryParams[1];
		Object[] values = (Object[]) queryParams[2];
		
		String orderBy = getOrderBy();
		if(StringUtil.isEmpty(orderBy)) {
			orderBy = "cn desc";
		}		
		List<Record> list=DBTool.findByMultProperties("EID_LDAP_USERS", properties, symbols, values,orderBy,getPager());
		
		//renderJson(list);
		renderDatagrid(list,DBTool.countByMultProperties("EID_LDAP_USERS", properties, symbols, values));
	}
}
