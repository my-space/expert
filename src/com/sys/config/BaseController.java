package com.sys.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.sys.mvc.user.SysUser;
import com.sys.tool.Pager;
import com.sys.tool.StringUtil;
/**
 * 自定义控制器基类
 * @author xpg
 *
 */
public class BaseController extends Controller {
	
	protected void renderDatagrid(Page<?> pageData) {
		Map<String, Object> datagrid = new HashMap<String, Object>();
		datagrid.put("list", pageData.getList());
		datagrid.put("count", pageData.getTotalRow());
		renderJson(datagrid);
	}
	
	protected void renderDatagrid(List<?> list, int total) {
		renderDatagrid(list, total, null);
	}
	protected SysUser getSessionUser() {
		return getSessionAttr("sysUser");
	}
	protected void renderDatagrid(List<?> list, int total, List<Map<String, Object>> footer) {
		Map<String, Object> datagrid = new HashMap<String, Object>();
		datagrid.put("list", list);
		datagrid.put("count", total);
		if(footer != null && footer.size() > 0) {
			datagrid.put("footer", footer);
		}
		renderJson(datagrid);
	}
	
	protected void renderDatagrid(List<Record> list) {
		Map<String, Object> datagrid = new HashMap<String, Object>();
		datagrid.put("list", list);
		renderJson(datagrid);
	}
	
	protected void renderSuccess(String msg) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("result", "success");
		result.put("msg", msg);
		renderJson(result);
	}
	
	protected void renderSuccess() {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("result", "success");
		renderJson(result);
	}
	
	protected void renderFailed(String msg) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("result", "fail");
		result.put("msg", msg);
		renderJson(result);
	}
	
	protected void renderFailed() {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("result", "fail");
		renderJson(result);
	}
	protected Pager getPager() {
		Pager pager = new Pager();
		pager.setPage(getParaToInt("pageNum", 0));
		pager.setRows(getParaToInt("pageSize", 0));		
		return pager;
	}
	protected Object[] getQueryParams() {
		List<String> properties = new ArrayList<String>();
		List<String> symbols = new ArrayList<String>();
		List<Object> values = new ArrayList<Object>();
		
		Map<String, String[]> paraMap = getParaMap();
		for (String paraName : paraMap.keySet()) {
				if(paraName.contains("pageNum")||paraName.contains("pageSize")) {
					continue;
				}
				else{
				String field = paraName;
				String symbol = "like";
				String value = paraMap.get(paraName)[0];
				
				//处理范围参数
				if(field.startsWith("_start_")) {
					field = field.replaceAll("^_start_", "");
					symbol = ">=";
				}else if(field.startsWith("_end_")) {
					field = field.replaceAll("^_end_", "");
					symbol = "<=";
				}				
				//全部搜索都以模糊搜索处理	
				if(StringUtil.isNotEmpty(value)){
				value = "%" + value + "%";
				properties.add(field);
				symbols.add(symbol);
				values.add(value);
				}
			}
		 }
		return new Object[]{properties.toArray(new String[]{}), symbols.toArray(new String[]{}), values.toArray(new Object[]{})};
	}
	
	protected String getOrderBy() {
		String sqlOrderBy = ""; 
		Map<String, String[]> paraMap = getParaMap();
		if(paraMap.get("sort") != null && paraMap.get("sort").length > 0) {
			String[] sort = paraMap.get("sort")[0].split(",");
			String[] order = paraMap.get("order")[0].split(",");
			sqlOrderBy = sort[0] + " " + order[0];
			for (int i = 1; i < sort.length; i++) {
				sqlOrderBy += ", " + sort[i] + " " + order[i];
			}
		}
		return sqlOrderBy;
	}}
