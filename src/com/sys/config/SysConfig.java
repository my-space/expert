package com.sys.config;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.dialect.SqlServerDialect;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.render.ViewType;
import com.jfinal.template.Engine;
import com.rdms.cms.config.CmsRoutes;
import com.rdms.cms.mvc.model.MapKit_Rdms;
import com.sys.handler.UrlHandler;
import com.sys.mvc._MappingKit;


public class SysConfig extends JFinalConfig {
	/**
	 * 配置JFinal常量
	 */
	@Override
	public void configConstant(Constants me) {
		//读取数据库配置文件
		PropKit.use("config.properties");
		//设置当前是否为开发模式
		me.setDevMode(PropKit.getBoolean("devMode"));
		//设置默认上传文件保存路径 getFile等使用
		me.setBaseUploadPath("upload/temp/");
		//设置上传最大限制尺寸
		//me.setMaxPostSize(1024*1024*10);
		//设置默认下载文件路径 renderFile使用
		me.setBaseDownloadPath("download");		
		//设置默认视图类型
		me.setViewType(ViewType.FREE_MARKER);
		//设置404渲染视图
		//me.setError404View("404.html");
	}
	/**
	 * 配置JFinal路由映射
	 */
	@Override
	public void configRoute(Routes me) {
		//me.add(new RdmsRoutes());
		me.add(new CmsRoutes());
	}
	/**
	 * 配置JFinal插件
	 * 数据库连接池
	 * ORM
	 * 缓存等插件
	 * 自定义插件
	 */
	@Override
	public void configPlugin(Plugins me) {
		//配置数据库连接池插件
		C3p0Plugin dbPlugin=new C3p0Plugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password"));
		//orm映射 配置ActiveRecord插件
		ActiveRecordPlugin arp=new ActiveRecordPlugin(dbPlugin);
		arp.setShowSql(PropKit.getBoolean("devMode"));
		dbPlugin.setDriverClass("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		arp.setDialect(new SqlServerDialect());
		arp.setContainerFactory(new CaseInsensitiveContainerFactory(true));
		/********在此添加数据库 表-Model 映射*********/
		//arp.addMapping("wms_instorage", InstorageModel.class);
		//添加到插件列表中
		_MappingKit.mapping(arp);
		MapKit_Rdms.mapping(arp);
		me.add(dbPlugin);
		me.add(arp);
	}
	/**
	 * 配置全局拦截器
	 */
	@Override
	public void configInterceptor(Interceptors me) {
		
	}
	/**
	 * 配置全局处理器
	 */
	@Override
	public void configHandler(Handlers me) {
		me.add(new UrlHandler());
		me.add(new ContextPathHandler("ctx"));
	}
	
	/**
	 * 配置模板引擎 
	 */
	@Override
	public void configEngine(Engine me) {
		//这里只有选择JFinal TPL的时候才用
		//配置共享函数模板
		//me.addSharedFunction("/view/common/layout.html")
	}
	@Override
	public void afterJFinalStart() {
		 /*try {
			setSharedVariable("ctx", JFinal.me().getContextPath());
		} catch (TemplateException e) {
			e.printStackTrace();
		}*/

	}	
	
	
	public static void main(String[] args) {
		JFinal.start("WebRoot", 80, "/", 5);
	}
	

}
