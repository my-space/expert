/**
 * @author Lion
 * @date 2017年1月24日 下午12:02:35
 * @qq 439635374
 */
package com.sys.mvc;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.sys.mvc.org.SysOrg;
import com.sys.mvc.user.SysUser;



public class _MappingKit {

	public static void mapping(ActiveRecordPlugin arp) {
		//系统管理
		arp.addMapping("eid_ldap_users","LDAP_ID",SysUser.class);//用户
		arp.addMapping("eid_ldap_groups","LDAP_ID",SysOrg.class);//用户角色
		//arp.addMapping("sys_menu", "id", SysMenu.class);//菜单		
	}
}

