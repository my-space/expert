/**
 * @author Lion
 * @date 2017年1月24日 下午12:02:35
 * @qq 439635374
 */
package com.sys.mvc.user;

import com.jfinal.kit.StrKit;
import com.sys.config.BaseController;
import com.sys.mvc.org.SysOrg;
import com.sys.tool.MD5Util;

/***
 * 用户管理控制器
 * @author Administrator
 *
 */

public class UserController extends BaseController {
	
	/***
	 * 获取管理首页
	 */
	public void getListPage(){
    	render("/WEB-INF/admin/user/list.html");
    }
	
    /***
     * 保存
     */
    public void save(){
    	SysUser o = getModel(SysUser.class);
    	if(StrKit.notBlank(o.getStr("ldap_id"))){
    		String password = o.getStr("password");
    		if(StrKit.isBlank(password)){//如果没传递密码
    			SysUser u = SysUser.dao.findById(o.getStr("Id"));
    			password = u.getStr("password");//获取原始密码
    			o.set("password",password);//设置为原始密码
    		}else{//传递了密码 , 设置新密码
    			
    			try {
					o.set("password",MD5Util.md5Digest(password));
				} catch (Exception e) {
					// TODO 自动生成的 catch 块
					e.printStackTrace();
				}//加密新密码
    		}
    		o.update();
    	}else{
    		
    		try {
				o.set("password",MD5Util.md5Digest(o.getStr("password")));
			} catch (Exception e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
			}//加密新密码
    		o.save();
    	}
    	renderSuccess();
    }
    /***
     * 获取编辑页面
     */
    public void getEditPage(){
    	//添加和修改
    	String id = getPara("username");
    	if(StrKit.notBlank(id)){//修改
    		SysUser o = SysUser.dao.getByUsername(id);
    		String orgid = o.getStr("");///ssssssssssssssss
    		SysOrg org = SysOrg.dao.getById(orgid);
    		setAttr("org", org);
    		setAttr("o", o);
    	}else{
    		String orgid = getPara("orgid");
    		SysOrg org = SysOrg.dao.getById(orgid);
    		setAttr("org", org);
    	}
    	render("/WEB-INF/admin/user/edit.html");
    }
    /***
     * 验证用户 , 是否被注册
     */
    public void validUsername(){
    	SysUser user = getModel(SysUser.class);
    	if(user!=null){
    		String username = user.getStr("ldap_id");
    		if(StrKit.notBlank(username)){
    			SysUser o =  SysUser.dao.getByUsername(username);
    			if(o==null){//用户不存在 
    				renderSuccess();
    			}else{
    				renderFailed();
    			}
    		}else{
    			renderSuccess();
    		}
    	}else{
    		renderSuccess();
    	}
    }
   
}
