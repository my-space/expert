package com.sys.tool;

import java.security.MessageDigest;

public class MD5Util {
	/** 
     * MD5数字签名 
     * @param src 
     * @return 
     * @throws Exception 
     */  
	public static String md5Digest(String src) throws Exception {  
       // 定义数字签名方法, 可用：MD5, SHA-1  
       MessageDigest md = MessageDigest.getInstance("MD5");  
       byte[] b = md.digest(src.getBytes("UTF-8"));  
       return byte2HexStr(b);  
    }  
	/** 
     * 字节数组转化为小写16进制字符串 
     * @param b 
     * @return 
     */  
    public static String byte2HexStr(byte[] b) {  
        StringBuilder sb = new StringBuilder();  
        for (int i = 0; i < b.length; i++) {  
            String s = Integer.toHexString(b[i] & 0xFF);  
            if (s.length() == 1) {  
                sb.append("0");  
            }  
            sb.append(s.toLowerCase());  
        }  
        return sb.toString();  
    }  
}